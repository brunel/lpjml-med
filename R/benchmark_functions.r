#####
# LPJ specific functions
#####

# reads lpj grid and returns a list with geographic extent,
# number of rows, number of columns, position in matrix for each point
read.grid <- function(grid,npixel,res=0.5,endian=.Platform$endian){
  grid.fn <- file(grid,"rb")
  seek(grid.fn, where=0, origin="start")
  grid.data <- matrix(readBin(grid.fn, integer(), n=npixel*2, size=2, endian=endian),ncol=2,byrow=T) / 100
  #grid extent
  ext.lon <- c(min(grid.data[,1]),max(grid.data[,1]))
  ext.lat <- c(min(grid.data[,2]),max(grid.data[,2]))
  nrow <- (ext.lat[2]-ext.lat[1])/res+1
  ncol <- (ext.lon[2]-ext.lon[1])/res+1
  # pixelarea in m2
  area <- (111.263*1000*res)*(111.263*1000*res)*cos(grid.data[,2]*pi/180.)
  #position in matrix for each point
  ilon <- as.integer((grid.data[,1]-ext.lon[1])/res + 1.01)
  ilat <- as.integer((grid.data[,2]-ext.lat[1])/res + 1.01)
  glon <- as.integer(grid.data[,1]/res-(-180+0.5*res)/res + 1.01)
  glat <- as.integer(grid.data[,2]/res-(-90+0.5*res)/res + 1.01)
  close(grid.fn)
  list(ext.lon=ext.lon,ext.lat=ext.lat,nrow=nrow,ncol=ncol,ilon=ilon,ilat=ilat,area=area,lon=grid.data[,1],lat=grid.data[,2],res=res,glat=glat,glon=glon)
}

# returns index of grid2-coordinates in grid1
match.grids <- function(grid1,grid2,res=0.5){
  lat1 <- (grid1$ilat-1)*res+grid1$ext.lat[1]
  lon1 <- (grid1$ilon-1)*res+grid1$ext.lon[1]
  lat2 <- (grid2$ilat-1)*res+grid2$ext.lat[1]
  lon2 <- (grid2$ilon-1)*res+grid2$ext.lon[1]
  ncols <- 360/res
  nrows <- 180/res
  map <- array(NA,dim=c(ncols,nrows))
  ilon <- as.integer((lon1-(-180+0.5*res))/res + 1.01)
  ilat <- as.integer((lat1-(-90+0.5*res))/res + 1.01)
  for(i in 1:length(lon1))
    map[ilon[i],ilat[i]] <- i
  index <- rep(NA,length(lon2))
  ilon <- as.integer((lon2-(-180+0.5*res))/res + 1.01)
  ilat <- as.integer((lat2-(-90+0.5*res))/res + 1.01)
  for (i in 1:length(lon2))
    index[i] <- map[ilon[i],ilat[i]]
  index
}# end (function match.grids)

# finds pixels in grid closest to a given array of longitudes/latitudes
find.nearest.pixel <- function(grid,lon,lat,res=0.5){
  la <- (grid$ilat-1)*res+grid$ext.lat[1]
  lo <- (grid$ilon-1)*res+grid$ext.lon[1]
  ind <- slice.index(la,1)
  index <- rep(0,length(lon))
  #for each point
  for(p in 1:length(lon)){
    latdiff <- abs(la-lat[p])
    londiff <- abs(lo-lon[p])
    index[p]=ind[londiff==min(londiff[ind[latdiff==min(latdiff)]]) & latdiff==min(latdiff)][1]
  }
  index
}

# extract data for some pixels from BSQ data
# returns a matrix with one column for each pixel
extract.pixeldata <- function(file,pixel,npixel,endband,startband=1,offset=0,data.type=numeric(),size=4,endian=.Platform$endian){
 
  data <- matrix(rep(0,length(pixel)*(endband-startband+1)),ncol=length(pixel))
  pos=(startband-1)*npixel*size+offset
  for(b in 1:(endband-startband+1)){
    seek(file, where=pos, origin="start")
    file.data <-  readBin(file, data.type, n=npixel, size=size, endian=endian) # data for just one month
    for(p in 1:length(pixel))
      data[b,p] <- file.data[pixel[p]]
    pos <- pos+npixel*size
  }#endfor (each band)
  data
}# end (extract.pixeldata)

# calculates area weighted global means
global.mean <- function(data,area,nbands=1){
  sum <- sum(data*rep(area,nbands))
  if(length(dim(data)>0)){
     if(dim(data)[1]==nbands)
        sum <- t(sum(t(data)*rep(area,nbands)))
   }
  (sum/sum(rep(area,nbands)))
}#end (global.mean)

lat.average <- function(data,grid,west=-180,east=180){
   lat <- seq(min(grid$lat),max(grid$lat),by=grid$res)
   mean <- rep(0,length(lat))
   count <- mean
   sd <- mean
   if(length(data)!=length(grid$lat)){
     print("lenght does not match in lat.average")
     0
   }else{
     for(l in 1:length(lat)){
       ind <- which(lat[l]==grid$lat & grid$lon >= west & grid$lon <= east)
       count[l] <- length(ind)
       mean[l] <- sum(data[ind])/length(ind)
       sd[l] <- sd(data[ind])
     }
     list(lat=lat,mean=mean,std=sd,count=count,east=east,west=west)
   }
}#end(lat.average)

######################
## quality measures ##
######################
N <- function (o, p) {
  if (length(o) == 0 || length(p) == 0) stop ("vector of length zero")
  if (length(o) != length(p) && length(p) != 1) stop ("incompatible dimensions")
  length(o)
}
Willmott <- function (o, p) {
  N(o, p)
  willmott <- 1 - sum((p - o)^2,na.rm=T) / sum((abs(p - mean(o,na.rm=T)) + abs(o - mean(o,na.rm=T)))^2,na.rm=T)
  willmott
}
EF <- function (o, p) {
  N(o, p)
  EF <- 1 - sum((p - o)^2) / sum((o - mean(o))^2)
  EF
}

# print text into header and footer
print.hf <- function(par.header,par.footer,header,footer=date()){
    savepar <- par(par.footer)
    plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
    text(0.4,0.5,lab=footer)
    par(par.header)
    plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
    title(header)
    par(savepar)
}
# draw a legend to an image plot
draw.legend <- function(ylim,col,label=""){
  mar <- par("mar")
  mar[4] <- 5
  mar[3] <- mar[3]+2
  savepar <- par(mar=mar)
  plot.new()
  plot.window(xlim=0:1, ylim=ylim)
  rect(0, seq(ylim[1],ylim[2], length=length(col))[-(length(col))],1, seq(ylim[1],ylim[2], length=length(col))[-1], col=col, border=NA)
  #rect(0,ylim[1] ,1, ylim[2], border="black")
  box()
  axis(4, las=1,line=0)
  mtext(label,3)
  par(savepar)
}

#finds zlim of either one data set or the difference of two datasets
find.zlim <- function(data1,grid1,data2=NULL,grid2=NULL,trunk=0.05,equaldist=TRUE){
    #do not plot extreme 5%
    res <- (grid1$ext.lat[2]-grid1$ext.lat[1])/(grid1$nrow-1)
    bands <- as.integer(length(data1)/ length(grid1$ilon))
    #if(length(data1) == length(grid1$ilon) && length(data2) == length(grid2$ilon)){
      img1 <- array(NA, dim=c(180/res,360/res))
      img2 <- img1
      data <- NA
      npixel1 <- length(grid1$ilon)
      for(b in 0:(bands-1)){
      if(length(data2)==0){
         for(p in 1:npixel1)
           img1[grid1$glat[p],grid1$glon[p]] <- data1[b*npixel1+p]
         data <- c(data,img1);
      }
      else{
        npixel2 <- length(grid2$ilon)
        for(p in 1:npixel1)
          img1[grid1$glat[p],grid1$glon[p]] <- data1[p]
        for(p in 1:npixel2)
          img2[grid2$glat[p],grid2$glon[p]] <- data2[p]
        
        #unequal grids
#        if(sum(grid1$ext.lat-grid2$ext.lat)!=0){
 #         if(grid1$ext.lat[1] != grid2$ext.lat[1]){
  #          if(grid1$ext.lat[1] < grid2$ext.lat[1]){
   #           tmp <- rbind(rep(NA,grid2$ncol),img2)
    #          img2 <- tmp
      #      }else{
     #         tmp <- rbind(rep(NA,grid1$ncol),img1)
       #       img1 <- tmp
        #    }
         # }
          #if(grid1$ext.lat[2] != grid2$ext.lat[2]){
           # if(grid1$ext.lat[2] < grid2$ext.lat[2]){
            #  tmp <- rbind(img1,rep(NA,grid1$ncol))
             # img1 <- tmp
            #}else{
             # tmp <- rbind(img2,rep(NA,grid2$ncol))
              #img2 <- tmp
           # }
         # }
       # }
        img.diff <- img2-img1
        q <- quantile(img.diff,c(trunk,1-trunk),na.rm=TRUE)
        #data <- c(data,img.diff)
        data <- c(data,q)
      }
    }
    #q <- quantile(data,c(trunk,1-trunk),na.rm=TRUE)
    if(equaldist==TRUE){
      c(-(max(abs(data),na.rm=T)),max(abs(data),na.rm=T))
    }else{
      c(min(data,na.rm=T),max(data,na.rm=T))
    }
  }


######
# table functions, adapted from http://addictedtor.free.fr/graphiques/graphcode.php?graph=28
######
make.table <- function(nr, nc) { 
    savepar <- par(mar=rep(0, 4), pty="m") 
    plot(c(0, nc*2 + 1), c(0, -(nr + 1)), 
         type="n", xlab="", ylab="", axes=FALSE)
    savepar
}

get.r <- function(i, nr)     i %% nr + 1 
get.c <- function(i, nr)     i %/% nr + 1 

draw.title <- function(title, i = 0, nr, nc) { 
    r <- get.r(i, nr) 
    c <- get.c(i, nr) 
    text((nc*2 + 1)/2, 0, title, font=2) 
}

draw.cell <- function(i, nr, string = NULL,...) {
    r <- get.r(i, nr) 
    c <- get.c(i, nr)   
    savepar <- par(...)
    col="white"
    font=1
    if(c==1 || r==1){ # heading
      col="grey"
      font=2
    }   
    rect((2*(c - 1) + .5), -(r - .5), (2*c + .5), -(r + .5),col=col)
    text((2*(c - 1) + .6), -(r), string,font=font,adj=c(0,0.5))
    par(savepar)
}
