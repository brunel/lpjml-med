#-------------------------------------------------------------------
# cal_helper.r
# R-Script prepare LPJ simulations with fixed LAIMAX 1 ... 7 for crop calibration,
# prepare calibration script, and construct new laimax parameter file.
#
# Tim Beringer
# 18.12.2013
#-------------------------------------------------------------------


# calibration workflow ####

# 1. check out trunk from SVN server
# 2. compile code
# 3. use this script to prepare lpj simulations with laimax 1 to 7
# 4. use lpjdistribute.sh to start simulations
# 5. call calibration script
# 6. create new laimax.par file with this script

closeAllConnections()
rm(list=ls(all=TRUE))
gc()

# RUN LPJ SIMULATIONS ####

#for(laimax ...)
#  system()

if(!file.exists(paste(d.fao,"output.yields",sep=""))) dir.create(paste(d.fao,"output.yields",sep=""))
setwd(paste(d.fao,"output.yields/",sep=""))

for(i in 1:7){
  if(!file.exists(paste(d.fao,"output.yields/","cftfrac.LAI",i,".bin",sep="")))
    file.symlink(paste(d.fao,"output.laimax.",i,"/cftfrac.bin",sep=""),paste(d.fao,"output.yields/","cftfrac.LAI",i,".bin",sep=""))
  
  if(!file.exists(paste(d.fao,"output.yields/","pft_harvest.LAI",i,".bin",sep="")))
    file.symlink(paste(d.fao,"output.laimax.",i,"/pft_harvest.pft.bin",sep=""),paste(d.fao,"output.yields/","pft_harvest.LAI",i,".pft.bin",sep=""))
}
if(!file.exists(paste(d.fao,"output.yields/grid.bin",sep="")))
  file.symlink(paste(d.fao,"output.laimax.1/grid.bin",sep=""),paste(d.fao,"output.yields/grid.bin",sep=""))

# PREPARE AND RUN CALIBRATION SCRIPT ####

y.first <- 1901
y.start <- 1999
y.end <- 2003
# p.masks <- "/data/biosx/LPJ/calib/" # place where cal script looks for lpjLandis0_mask.img and countryMask.img
# p.input <- "/iplex/01/2012/open/input_VERSION2/" # directory where cal script looks for input grid.bin and cow_mg_2006.bin
# p.output <- "/iplex/01/2012/open/Lena/LPJ_Oct2014_RCP/output.yields/" # directory where cal script looks for results from LAIMAX 1-7 simulations
p.masks <- "/mnt/biosx/LPJ/calib/" # place where cal script looks for lpjLandis0_mask.img and countryMask.img
p.input <- "/mnt/open/input_VERSION2/" # directory where cal script looks for input grid.bin and cow_mg_2006.bin
p.output <- "/mnt/open/Lena/LPJ_Oct2014_RCP/output.yields/" # directory where cal script looks for results from LAIMAX 1-7 simulations
f.path  <- "/mnt/biosx/fao_data/download_090203/"

h.size.grid <- 43 # size of input data header in bytes, old version has size 35
h.size.country <- 43 # size of input data header in bytes, old version has size 39

# add part that changes paths, etc. in calibration script
f.cal <- readLines(paste(d.base,"R/global_fao_vs_lpjml_calib.r",sep=""))
f.cal[grep("# no of cells recorded in file",f.cal)] <- "ncell <- 67420 # no of cells recorded in file"
f.cal[grep("#first year to evaluate",f.cal)] <- paste("startyear <- ",y.start-y.first+1, " #first year to evaluate",sep="")
f.cal[grep("#last year to evaluate",f.cal)] <- paste("endyear <- ",y.end-y.first+1, " #last year to evaluate",sep="")
f.cal[grep("fao.years",f.cal)[1]] <- paste("fao.years <- c(",y.start,":",y.end,")",sep="")
f.cal[grep("# set script working path",f.cal)+1] <- paste("script.path <- \"",p.masks,"\"",sep="")
f.cal[grep("lpjinput.path <- ",f.cal)] <- paste("lpjinput.path <- \"",p.input,"\"",sep="")
f.cal[grep("lpjoutput.path <- ",f.cal)] <- paste("lpjoutput.path <- \"",p.output,"\"",sep="")
f.cal[grep("fao.path <- ",f.cal)] <- paste("fao.path <- \"",f.path,"\"",sep="")
f.cal[grep(glob2rx("seek(file.grid.input*"),f.cal)] <- paste("seek(file.grid.input,where=",h.size.grid,",start=\"origin\")",sep="")
f.cal[grep(glob2rx("seek(file.country.input*"),f.cal)] <- paste("seek(file.country.input,where=",h.size.country,",start=\"origin\")",sep="")

writeLines(f.cal,paste(paste(d.base,"R/global_fao_vs_lpjml_calib.r",sep="")))

# run calibration script ####
source(paste(d.base,"R/global_fao_vs_lpjml_calib.r",sep=""))

# CREATE MODIFIED PARAM_LAIMAX.PAR ####

f.param.laimax.par.h <- readLines(paste(d.par,"manage_laimax_alphaa_revision_1241_sc.par",sep=""),n=16)
f.param.laimax.par <- read.table(paste(d.par,"manage_laimax_alphaa_revision_1241_sc.par",sep=""),skip=16)

f.laimax.new <- read.table(paste(d.cal,"copy_me_by_hand_to_manage_laimax_alphaa_revision_1200_sc.par.txt",sep=""))
#f.laimax.new <- read.table(paste("~/Documents/PIK/copy_me_by_hand_to_manage_laimax_alphaa_revision_1200_sc.par.txt",sep=""))


for(i in 1:197){
  f.param.laimax.par[i,3:14] <- f.laimax.new[i,]  
}
# rename 'Last change' in par header
f.param.laimax.par.h[8]<-  paste("/**    Last change: ",format(Sys.Date(), "%d.%m.%Y"),", ",my.name,"                         **/\t\t\t")

# add revision to file name, climate data to header, cal time frame
rev <- system(paste("svnversion ",d.host,sep=""), intern = TRUE)

paste(d.par,"manage_laimax_alphaa_fao.y.",y.start,".",y.end,"_date.",Sys.Date(),"_lpj.rev.",rev,".par",sep="")

writeLines(f.param.laimax.par.h,paste(d.par,"manage_laimax_alphaa_fao.y.",y.start,".",y.end,"_date.",Sys.Date(),"_lpj.rev.",rev,".par",sep=""))
write.table(f.param.laimax.par,paste(d.par,"manage_laimax_alphaa_fao.y.",y.start,".",y.end,"_date.",Sys.Date(),"_lpj.rev.",rev,".par",sep=""), append=T, sep = "\t", col.names=FALSE, row.names=FALSE, quote=c(2))

