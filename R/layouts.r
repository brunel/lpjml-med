# page layouts
layout.title <- list(layout=matrix(c(2,3,4,1),ncol=1,byrow=TRUE),widths=c(1,1),heights=c(1.5,8,1,1))
layout.sums <- list(layout=matrix(c(2,2,3,4,5,6,7,8,1,1),ncol=2,byrow=TRUE),widths=c(1,1),heights=c(1,3,3,3,1))
layout.maps <- list(layout=matrix(c(2,2,2,2,3,4,5,6,7,8,9,10,11,12,13,14,1,1,1,1),ncol=4,byrow=TRUE),widths=c(5,1.5,5,1.5),heights=c(1,3,3,3,1))
layout.flux <- list(layout=matrix(c(2,2,3,4,5,6,7,8,9,10,1,1),ncol=2,byrow=TRUE),widths=c(1,1,1),heights=c(2,3,3,3,3,1))

page.layout <- function(l){
  layout(l$layout,l$widths,l$heights)
}

# parameter settings
par.footer <- list(mar=c(0,0,1,0),cex=1,adj=0)
par.table <- list(mar=c(0,0,1,0),cex=0.9,adj=0.5)
par.title <- list(mar=c(1,1,4,1),cex.main=2,adj=0.5)
par.title2 <- list(mar=c(0,0,3,0),cex.main=2,adj=0.5)
par.maps <- list(mar=c(1,1,4,1),cex.main=1.5,adj=0.5)
par.img <- list(mar=c(5,5,4,2),cex.main=1.5,adj=0.5)
