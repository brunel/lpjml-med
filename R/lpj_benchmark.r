#-------------------------------------------------------------------
# lpj_benchmark.r
# R-Script to compare LPJ output with standard output and validation data
# output to pdf file
# requires files "benchmark_functions.r", "benchmark.settings", "layouts.r"
# call R CMD BATCH [--conf="yoursettings.filename"] lpj_benchmark.r from the command line
#  or source("lpj_benchmark.r") from a running R process
# change benchmark.settings to set path names etc
# the script contains 5 main parts:
#  - settings and initializations
#  - global sums and means
#  - difference maps
#  - comparison with eddy covariance flux data
#  - comparison with FAO yield data
# U. Heyder
# 01.07.2009
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# PART 1
# Settings and initializations
#-------------------------------------------------------------------

# deletes all variables
rm(list=ls(all=TRUE))

# load required libaries
require(fields)
require(maps)

# load functions for this script
source("benchmark_functions.r")
# load settings
if(!is.na(x <- pmatch("--conf",commandArgs()))){
  print(commandArgs())
  source(substring(commandArgs()[x],8))
}else{
 source("benchmark.settings")
}
source("layouts.r")

plot.timeseries <- 1
plot.diff <- 1

# further settings
sizeof.data <- 4         # 4 for LPJ output, 2 for climate data
data.type <- numeric()   # numeric() for LPJ output, integer() for climate data
offset <- 0              # 0 for output data, headersize for climate data
lastday <- c(31,28,31,30,31,30,31,31,30,31,30,31)
freemem <- FALSE
if(isnatural==0){
  nreg <- 200              
  nbands.crops <- 32
  ncft <- 12 #skipping gras & others
  n.country.fao <- 351
  n.country.lpjml <- 197
  cropbands <- c(1,2,3) # which cfts to plot
  bands <- c(cropbands,cropbands+nbands.crops/2)
}

# files to compare
files.out <- c("vegc.bin","soilc.bin","litc.bin","firec.bin","flux_estab.bin","mgpp.bin","mnpp.bin","mrh.bin","mevap.bin","mtransp.bin","mrunoff.bin","fpc.bin")
single.bands <- c(rep(0,11),1)  # display bands indiviually
global.scale <- c(rep(1e-15,5),rep(1e-15*12,6),0) # scale for global outputs, 0 for global average, global sums otherwise
plot.global <- c(rep(1,11),0)
if(isnatural==0){
  harea.fn <- "cftfrac.bin"
  harvest.fn <- "pft_harvest.pft.bin"
  sdate.fn <- "sdate.bin"
  files.out <- c(files.out,harvest.fn,harea.fn,sdate.fn)
  single.bands <- c(single.bands,2,3,2)
  global.scale <- c(global.scale,1e-12/0.45,1e-10,0)
  plot.global <- c(plot.global,1,1,0)
}

# test whether files exist
f <- c(grid.fn.out,grid.fn.b,paste(path.out,files.out,sep=""),paste(path.b,files.out,sep=""),nep.fn,wf.fn)
if(isnatural==0){
  f <- c(f,cow.fn,fao.path,fao.countryfile)
}
if(isdischarge==1){
  f <- c(f,dis.fn,paste(path.out,"mdischarge.bin",sep=""),paste(path.b,"mdischarge.bin",sep=""))
}
fex <- file.exists(f)
if(sum(fex)<length(fex)){
  stop(paste("Cannot open file",f[fex==FALSE],"\n"))
}


# files for Eddy-Flux comparison
file.npp.n <- "mnpp.bin"
file.rh.n <- "mrh.bin"
file.intc.n <- "minterc.bin"
file.evap.n <- "mevap.bin"
file.transp.n <- "mtransp.bin"
file.gpp.n <- "mgpp.bin"
file.dis.n <- "mdischarge.bin"

# titles and labels
units <- c(rep("gC/m2",3),rep("gC/m2/year",5),rep("mm/year",3),"")
units.global <- c(rep("GtC",3),rep("GtC/year",5),rep("10³ km3/year",3),"")
names.out <- c("Vegetation\nCarbon","Soil\nCarbon","Litter\nCarbon","Fire\nCarbon","Establishment\nflux","GPP","NPP","heterotrophic\nRespiration","Evaporation","Transpiration","Runoff","FPC")
pftnames <- c("NVfrac","TrBEG","TrBRG","TeNEG","TeBEG","TeBSG","BorNEG","BorBSG","C3","C4","VegCov")
if(isnatural==1){
 lit.estimates <- c("tbd","2376–2456 (1)\n1576 (2)\n1395 (3)","tbd","tbd","tbd","tbd","74.8 (7)",rep("tbd",5))
}else{
  lit.estimates <- c("460-660 (4,5,6)","2376–2456 (1)\n1576 (2)\n1395 (3)","tbd","2.2-3.87 (8-11)","tbd","123+/-8 (14)","66.05 (7)\n62.6 (5)\n49.52-59.74 (13)",rep("tbd",5),rep(c("524.08 (12)\nirr+nirr","492.66 (12)\nirr+nirr","498.33 (12)\nirr+nirr"),2),rep(c("231.84 (12)\nirr+nirr","150.11 (12)\nirr+nirr","138.85 (12)\nirr+nirr"),2),rep("tbd",6))
}
lit.source <- c("(1) Batjes et al. 1996","(2) Eswaran et al. 1993","(3) Post et al. 1982","(4) Olson et al. 1985","(5) Saugier et al. 2001","(6) WBGU 1998","(7) Vitousek et al. 1986","(8) Seiler & Crutzen 1980","(9) Andreae & Merlet 2001","(10) Ito & Penner 2004","(11) van der Werf et al. 2004","(12) FAOSTAT 1990-2000","(13) Ramakrishna et al. 2003","(14) Beer et al. 2010")
# BIOENERGY
if(isnatural==0){
  cropnames <- c("wheat","rice","maize","millet","fpea","sugarbeet","cassava","sunflower","soybeans","groundnuts","rapeseed","sugarcane","others","grasses")
  FRESHMATTER <- 100 / c(88, 87, 88, 88, 90, 24, 35, 93, 91, 94, 92, 27, 100, 100)  # cassava (from Wirsenius (2000) PhD-Thesis, dummy values for grass & others)

  units <- c(units,rep("gC/m2",length(bands)),rep("",length(bands)),rep("DOY",length(bands)))
  units.global <- c(units.global,rep("Gt DM/year",length(bands)),rep("ha",length(bands)),rep("DOY",length(bands)))
  #names.out <- c(names.out,paste("Production",rep(cropnames[cropbands],2),"\n",c(rep("rainfed",length(cropbands)),rep("irrigated",length(cropbands)))),paste("harea",rep(cropnames[cropbands],2),"\n",c(rep("rainfed",length(cropbands)),rep("irrigated",length(cropbands)))),rep(paste("sowing date",cropnames[cropbands]),2))
  names.out <- c(names.out,paste("Prod.",rep(cropnames[cropbands],2),"\n",c(rep("rainfed",length(cropbands)),rep("irrigated",length(cropbands)))),paste("harea",rep(cropnames[cropbands],2),"\n",c(rep("rainfed",length(cropbands)),rep("irrigated",length(cropbands)))),paste("sowing date",rep(cropnames[cropbands],2),"\n",c(rep("rainfed",length(cropbands)),rep("irrigated",length(cropbands)))))
}

title <- ifelse(isnatural==1,"PNV","actual vegetation")
diff.map.title <- ifelse(plot.abs==0,"Run - Benchmark run","Run")

summarytable.headings <- c("","Literature\nestimates","benchmark\nrun","run","diff","%")


# limits for data display
set.zlim <- 0 #0=automatic, 1=disp.zlim as limit
disp.zlim=c(0,1)
set.ylim <- 0
disp.ylim=c(-90,90)
set.xlim <- 0
disp.xlim=c(-180,180)

# determine data characteristics from filesize
npixel.b <- file.info(grid.fn.b)$size/4
npixel.out <- file.info(grid.fn.out)$size/4
nyear.out <-  file.info(paste(path.out,"vegc.bin",sep=""))$size/sizeof.data/npixel.out
nyear.b <- file.info(paste(path.b,"vegc.bin",sep=""))$size/sizeof.data/npixel.b
nyear <- min(nyear.b,nyear.out)
nrefyear <- refyears[2]-refyears[1]+1
date <- date()           # timestamp
tsyears <- c(1,nyear)
if(plot.timeseries==0) tsyears <- refyears-startyear
# set colors
col <- rev(colorRampPalette(c("dark blue","cyan","green","chartreuse","grey90","yellow","orange","red","dark red"),bias=1)(100))

#read grids
grid.out <- read.grid(grid.fn.out,npixel.out,res,endian.out)
grid.b <- read.grid(grid.fn.b,npixel.b,res,endian.b)
if(isnatural==0){
  npixel.in <- file.info(grid.fn.in)$size/4
  grid.in <- read.grid(grid.fn.in,npixel.in,res,endian.in)
  # match input / output grids
  index.out <- match.grids(grid.in,grid.out,res)
  index.b <- match.grids(grid.in,grid.b,res)
  # open files with harvested area
  file.hfrac.out <- file(paste(path.out,harea.fn,sep=""),"rb")
  file.hfrac.b <- file(paste(path.b,harea.fn,sep=""),"rb")
}
# x and y dimensions for image plots
if(!set.xlim) disp.xlim <- grid.b$ext.lon
if(!set.ylim) disp.ylim <- grid.b$ext.lat

# create empty arrays
img.out <- array(NA, dim=c(180/res,360/res))
img.b <- img.out
img.diff <- img.out
timeseries.out <- matrix(rep(0,nyear*length(files.out)),nrow=nyear)
timeseries.b <- matrix(rep(0,nyear*length(files.out)),nrow=nyear)
if(isnatural==0){
  timeseries.crops.out <- array(rep(0,nyear*length(single.bands[single.bands>=2])*nbands.crops),dim=c(nyear,length(single.bands[single.bands>=2]),nbands.crops))
  timeseries.crops.b <- array(rep(0,nyear*length(single.bands[single.bands>=2])*nbands.crops),dim=c(nyear,length(single.bands[single.bands>=2]),nbands.crops))
  harea.out <- array(rep(0,nyear*length(single.bands[single.bands>=2])*nbands.crops),dim=c(nyear,length(single.bands[single.bands>=2]),nbands.crops))
  harea.b <- array(rep(0,nyear*length(single.bands[single.bands>=2])*nbands.crops),dim=c(nyear,length(single.bands[single.bands>=2]),nbands.crops))
}


#-------------------------------------------------------------------
# PART 2
# global means
#-------------------------------------------------------------------
#######
# calculate global means for each file
#######
cropcount <- 0
for (i in 1:length(files.out)){
  if(single.bands[i]>=2)
    cropcount <- cropcount+1
  if(plot.global[i]==0)
    next
  
  # initializations
  file.n <- files.out[i]
  nbands <- file.info(paste(path.b,file.n,sep=""))$size/sizeof.data/npixel.b/nyear.b
  print(file.n)
  # open files
  file.out <- file(paste(path.out,file.n,sep=""),"rb")
  file.b <- file(paste(path.b,file.n,sep=""),"rb")
  
  # loop over years
  for (y in tsyears[1]:tsyears[2]){#1:nyear){
    # read data
    # all pixels of one band in a row
    # time series for a pixel in a column
    seek(file.out, where=offset+(y-1)*npixel.out*nbands*sizeof.data, origin="start")
    seek(file.b, where=offset+(y-1)*npixel.b*nbands*sizeof.data, origin="start")
    file.data.out <-  matrix(readBin(file.out, data.type, n=npixel.out*nbands, size=sizeof.data, endian=endian.out),ncol=npixel.out,nrow=nbands,byrow=T) # data for just one year
    file.data.b <-  matrix(readBin(file.b, data.type, n=npixel.b*nbands, size=sizeof.data, endian=endian.b),ncol=npixel.b,nrow=nbands,byrow=T)
    #convert fire return intervall to fraction of gridcell burnt
    if(file.n=="firef.bin"){
      file.data.out <- 1/file.data.out
      file.data.b <- 1/file.data.b
    }
    if(single.bands[i]>=2){ # scale crops individually with harvested area
      # read harvested area
      pos <- offset+(y-1)*npixel.out*nbands*sizeof.data
      seek(file.hfrac.out,where=pos,origin="start")
      pos <- offset+(y-1)*npixel.b*nbands*sizeof.data
      seek(file.hfrac.b,where=pos,origin="start")
      hfrac.out <- matrix(readBin(file.hfrac.out, data.type, n=npixel.out*nbands.crops, size=sizeof.data, endian=endian.out),ncol=npixel.out,nrow=nbands.crops,byrow=T)
      hfrac.b <- matrix(readBin(file.hfrac.b, data.type, n=npixel.b*nbands.crops, size=sizeof.data, endian=endian.b),ncol=npixel.b,nrow=nbands.crops,byrow=T)
      for(b in 1:nbands.crops){
        band <- b
        if(band > nbands){ # sowing date has only 11 bands
          band <- b-nbands.crops/2
          if (band > nbands | band <1)
            next
        }
        if(single.bands[i]==2){
          timeseries.crops.out[y,cropcount,band] <-timeseries.crops.out[y,cropcount,band]+ global.mean(file.data.out[band,],hfrac.out[b,]*grid.out$area,1)
          timeseries.crops.b[y,cropcount,band] <- timeseries.crops.b[y,cropcount,band]+global.mean(file.data.b[band,],hfrac.b[b,]*grid.b$area,1)
          harea.out[y,cropcount,band] <- harea.out[y,cropcount,band]+sum(hfrac.out[b,]*grid.out$area)
          harea.b[y,cropcount,band] <- harea.b[y,cropcount,band]+sum(hfrac.b[b,]*grid.b$area)
        } else{
          timeseries.crops.out[y,cropcount,band] <-timeseries.crops.out[y,cropcount,band]+ global.mean(file.data.out[band,],grid.out$area,1)
          timeseries.crops.b[y,cropcount,band] <- timeseries.crops.b[y,cropcount,band]+global.mean(file.data.b[band,],grid.b$area,1)
          harea.out[y,cropcount,band] <- harea.out[y,cropcount,band]+sum(grid.out$area)
          harea.b[y,cropcount,band] <- harea.b[y,cropcount,band]+sum(grid.b$area)
        }
        
      }
    } else{
      # calculate global mean value for this year
      timeseries.b[y,i] <- global.mean(file.data.b,grid.b$area,nbands)
      timeseries.out[y,i] <- global.mean(file.data.out,grid.out$area,nbands)
    }
  } #endfor each year
  close(file.b)
  close(file.out)
}#endfor (outfiles)

# create summary data for reference years
years <- (refyears[1]-startyear):(refyears[2]-startyear)
b <- colMeans(timeseries.b[years,])*ifelse(global.scale==0,1,sum(grid.b$area)*global.scale)
d <- colMeans(timeseries.out[years,])*ifelse(global.scale==0,1,sum(grid.out$area)*global.scale)
plot.g <- plot.global
ind <- which(single.bands>=2)
if(length(ind)>0){
  b <- b[-ind]
  d <- d[-ind]
  plot.g <- plot.global[-ind]
  e <- colMeans(timeseries.crops.out[years,,bands])
  f <- colMeans(timeseries.crops.b[years,,bands])
  # account for single bands in crop output
  if(dim(e)[1]>0){
    for(x in 1:dim(e)[1]){
      b <- c(b,f[x,]*ifelse(global.scale[ind[x]]==0,1,colMeans(harea.b[years,x,bands])*global.scale[ind[x]]))
      d <- c(d,e[x,]*ifelse(global.scale[ind[x]]==0,1,colMeans(harea.out[years,x,bands])*global.scale[ind[x]]))
      plot.g <- c(plot.g,rep(plot.global[ind[x]],length(bands)))
    }
  }
}
gc1 <- matrix(c(plot.g,paste(names.out,"\n[",units.global,"]",sep=""),lit.estimates,formatC(c(b,d),digits=3,format="f"),formatC(c(d-b,(d-b)*100/b),digits=3,format="f",flag="+")),ncol=7)
tmp <- gc1[gc1[,1]==1,2:7]
gc1 <- rbind(summarytable.headings,tmp)

#########
# start plotting
########
# open pdf
if(compress.pdf==1){
  postscript("tmp.ps",width=8.2,height=11.6,family = "NimbusSan",horizontal=F)
}else{
  pdf(pdfname,width=8.2,height=11.6,family = "NimbusSan",pointsize=10)
}
# summary statistics on title page
l <- layout.title
tcol <- dim(gc1)[2]

c <- 0
o <- 1
while (c < sum(plot.g)){
  nr <- min(12,sum(plot.g)-c)+1
  page.layout(l)
  print.hf(par.title2,par.footer,"LPJ Benchmark",date)
  par(par.title2)
  text(0,0,adj=c(0,0),lab=(paste(ifelse(isnatural,"Natural vegetation\n","Landuse\n"),"Benchmark run: ",path.b,"\nRun: ",path.out,"\nDescription: ",run.descrip,sep="")),cex=1.5)

  par(par.table)
  make.table(nr,tcol)
  draw.title(paste("global sums,",title,refyears[1],"-",refyears[2]),i=0,nr,tcol)
  #for (i in 0:(length(gc1)-1))
   # draw.cell(i,nr,gc1[i+1],col="black",adj=0.25)
  for (i in seq(0,(nr*tcol-1),by=nr))
    draw.cell(i,nr,gc1[i%%nr+1,as.integer(i/nr)+1],col="black")
  for (i in 0:(nr*tcol-1)){
    if(i%%nr>0)
      draw.cell(i,nr,gc1[i%%nr+c+o,as.integer(i/nr)+1],col="black")
  }
  par(par.footer)
  plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
  # literature
  test <- ""
  x <- 0
  lrow <- 4
  step <- 1/(as.integer(length(lit.source)/lrow)+1)
  for(i in 1:length(lit.source)){
    test <- paste(test,lit.source[i],"\n",sep="")
    if(i%%lrow==0|i==length(lit.source)){
      text(x,1,adj=c(0,1),lab=test,cex=0.8)
      x <- x+step
      test <- ""      
    }
  }
  c <- c+nr
  o <- o-1
} #end while
  
# global time series
if(plot.timeseries){
l <- layout.sums
page.layout(l)
count <- 1
par(par.img)
factor.b <- ifelse(global.scale==0,1,sum(grid.b$area)*global.scale)
factor.out <- ifelse(global.scale==0,1,sum(grid.out$area)*global.scale)
b <- timeseries.b
d <- timeseries.out
if (length(ind)>0){
  b <- b[,-ind]
  d <- d[,-ind]
  factor.b <- factor.b[-ind]
  factor.out <- factor.out[-ind]
  e <- timeseries.crops.out[,,bands]
  f <- timeseries.crops.b[,,bands]
  # create band specific output for crop files
  for(x in 1:dim(e)[2]){
      b <- cbind(b,f[,x,])
      factor.b <- c(factor.b,rep(ifelse(global.scale[ind[x]]==0,1,colSums(harea.b[,x,bands])*global.scale[ind[x]]),length(bands)))
      d <- cbind(d,e[,x,])
      factor.out <- c(factor.out,rep(ifelse(global.scale[ind[x]]==0,1,colSums(harea.out[,x,bands])*global.scale[ind[x]]),length(bands)))
  }
}
for(i in 1:dim(b)[2]){
  if(plot.g[i]==1){   
    if(count%%max(l$layout)==1){
      print.hf(par.title2,par.footer,paste("Global sum,",title),date)
      plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
      legend(0.2,1,legend=c("benchmark run","run"),lty=c(1,2),title="global sum",cex=1.5)
      count <- count+3    
    }
    plot(seq(startyear,startyear+nyear-1),b[,i]*factor.b[i],ylim=c(min(b[,i],d[,i]),max(b[,i],d[,i]))*factor.b[i],lty=1,type="l",ylab=units.global[i],xlab="Year")
    title(names.out[i])
    points(seq(startyear,startyear+nyear-1),d[,i]*factor.out[i],lty=2,type="l")
    count <- count+1
  }#endif (plot.global[i]==1)
}#endfor each file
# remove temporary variables
if(freemem==TRUE)
  rm(timeseries.b,timeseries.out,b,d,gc1,tmp,factor.b,factor.out)
}# end plot timeseries

#-------------------------------------------------------------------
# PART 3
# difference maps
#-------------------------------------------------------------------
if(plot.diff==1){
par(par.maps)
l <- layout.maps
page.layout(l)
count <- 1
j <- 1
# reread files
for (i in 1:length(files.out)){
  # initializations
  file.n <- files.out[i]
  
  # open files
  file.out <- file(paste(path.out,file.n,sep=""),"rb")
  file.b <- file(paste(path.b,file.n,sep=""),"rb")
  seek(file.out, where=offset, origin="start")
  seek(file.b, where=offset, origin="start")
  if(length(grep("sdate",file.n))>0){
    size <- 2
    type <- integer()
  }else{
    size <- sizeof.data
    type <- data.type
  }
  nbands <- file.info(paste(path.b,file.n,sep=""))$size/size/npixel.b/nyear.b
  mean.out <- rep(0,npixel.out*nbands)
  mean.b <- rep(0,npixel.b*nbands)
  # loop over years
  for (y in (refyears[1]-startyear):(refyears[2]-startyear)){
    # read data
    # all pixels of one band in a row
    # time series for a pixel in a column
    seek(file.out, where=offset+y*npixel.out*nbands*size, origin="start")
    seek(file.b, where=offset+y*npixel.b*nbands*size, origin="start")
    file.data.out <-  matrix(readBin(file.out, type, n=npixel.out*nbands, size=size, endian=endian.out),ncol=npixel.out,nrow=nbands,byrow=T) # data for just one year
    file.data.b <-  matrix(readBin(file.b, type, n=npixel.b*nbands, size=size, endian=endian.b),ncol=npixel.b,nrow=nbands,byrow=T)
    #convert fire return intervall to fraction of gridcell burnt
    if(file.n=="firef.bin"){
      file.data.out <- 1/file.data.out
      file.data.b <- 1/file.data.b
    }
    tmp <- mean.out + file.data.out / nrefyear
    mean.out <- tmp
    tmp <- mean.b + file.data.b / nrefyear
    mean.b <- tmp
  } #endfor each year
  close(file.b)
  close(file.out)
  
  # create rectangular images
  if(single.bands[i]==0){
    plot.out <- array(rep(0,npixel.out),dim=c(1,npixel.out))
    plot.b <- array(rep(0,npixel.b),dim=c(1,npixel.b))
    for (b in seq(1,nbands)){
      plot.out <- plot.out+mean.out[b,]
      plot.b <- plot.b+mean.b[b,]
    }
    plotbands <- 1
    trunk <- 0.01
  }else if(single.bands[i]==1){
    plot.out <- mean.out
    plot.b <- mean.b
    plotbands <- 1:nbands
    trunk <- 0.01
    if(length(grep("fpc",file.n))>0){
      plot.out <- rbind(mean.out,colSums(mean.out[2:nbands,]))
      plot.b <- rbind(mean.b,colSums(mean.b[2:nbands,]))
      plotbands <- 1:(nbands+1)
    }
  }else if(single.bands[i]>=2){
    # plot selected bands
    plotbands <- bands[bands<=nbands]
    plot.out <- mean.out
    plot.b <- mean.b
    trunk=0.0005
  }
  if(plot.abs==0){
    disp.zlim <- find.zlim(plot.out,grid.out,plot.b,grid.b,trunk=trunk)
  }else{
    disp.zlim <- find.zlim(plot.out,grid.out,trunk=trunk,equaldist=FALSE)
  }
  for (b in plotbands){
    for(p in seq(1,npixel.out))
      img.out[grid.out$glat[p],grid.out$glon[p]] <- plot.out[b,p]
    for(p in seq(1,npixel.b))
      img.b[grid.b$glat[p],grid.b$glon[p]] <- plot.b[b,p]
    
    if(plot.abs==0){
      img.diff <- img.out-img.b
    }else{
      img.diff <- img.out
    }
    
    img.diff <- ifelse(img.diff<disp.zlim[1],disp.zlim[1],img.diff)
    img.diff <- ifelse(img.diff>disp.zlim[2],disp.zlim[2],img.diff)
  
    if(count%%max(l$layout)==1){
      # new page, print header and  footer
      print.hf(par.title2,par.footer,diff.map.title,date)
      count <- count+2
    } 
    # plot difference maps
    image(x=seq((-180+0.5*res),(180-0.5*res),by=res), y=seq((-90+0.5*res),(90-0.5*res),by=res),z=t(img.diff), zlim=disp.zlim, xlim=disp.xlim, ylim=disp.ylim,xlab="",ylab="",asp=1,col=(col),tcl=-0.2,yaxp=c(disp.ylim,4),xaxp=c(disp.xlim,8),labels=F,axes=T)
    map(xlim=disp.xlim,ylim=disp.ylim,add=T,boundary=T)
    ititle <- ifelse(length(plotbands)>1,paste(refyears[1],"-",refyears[2],"band",b,sep=" "),paste(refyears[1],"-",refyears[2],sep=" "))
    t <- ifelse(length(grep("fpc",file.n))>0,paste(names.out[j],pftnames[b]),names.out[j])
    title(t,cex.main=1.5)
    title(sub=ititle,line=0)
    draw.legend(disp.zlim,col,units[j])
    if(single.bands[i]>=2)
      j <- j+1
    count <- count+2
  }#endfor (each band)
  if(single.bands[i]<2)
    j <- j+1
}#endfor (outfiles)
# remove temporary variables
if(freemem==TRUE)
  rm(mean.out,mean.b,plot.out,plot.b,tmp)
} #end plot difference maps


#-------------------------------------------------------------------
# PART 4
# comparison to fluxdata
#-------------------------------------------------------------------
# open LPJ files
file.npp.out <- file(paste(path.out,file.npp.n,sep=""),"rb")
file.rh.out <- file(paste(path.out,file.rh.n,sep=""),"rb")
file.npp.b <- file(paste(path.b,file.npp.n,sep=""),"rb")
file.rh.b <- file(paste(path.b,file.rh.n,sep=""),"rb")
file.evap.out <- file(paste(path.out,file.evap.n,sep=""),"rb")
file.transp.out <- file(paste(path.out,file.transp.n,sep=""),"rb")
file.intc.out <- file(paste(path.out,file.intc.n,sep=""),"rb")
file.evap.b <- file(paste(path.b,file.evap.n,sep=""),"rb")
file.transp.b <- file(paste(path.b,file.transp.n,sep=""),"rb")
file.intc.b <- file(paste(path.b,file.intc.n,sep=""),"rb")
if(exists("gpp.fn")) file.gpp.out <- file(paste(path.out,file.gpp.n,sep=""),"rb")
if(exists("gpp.fn")) file.gpp.b <- file(paste(path.b,file.gpp.n,sep=""),"rb")
if(isdischarge==1){
  file.dis.out <- file(paste(path.out,file.dis.n,sep=""),"rb")
  file.dis.b <- file(paste(path.b,file.dis.n,sep=""),"rb")
}

########
# lists with titles etc for each variable
#######
if(exists("nep.fn")) fluxdata.nep <- list(fluxname=nep.fn,unit="gC/m2/month",title="NEP",des=nep.description)
if(exists("gpp.fn")) fluxdata.gpp <- list(fluxname=gpp.fn,unit="gC/m2/month",title="GPP",des=gpp.description,)
if(exists("rh.fn")) fluxdata.rh <- list(fluxname=rh.fn,unit="gC/m2/month",title="RH",des=rh.description)
if(exists("wf.fn")) fluxdata.wf <- list(fluxname=wf.fn,unit="mm/month",title="Evaporation + Interception",des=wf.description)
if(exists("dis.fn")) fluxdata.dis <- list(fluxname=dis.fn,unit="km3/month",title="River discharge",des=dis.description)
fluxvars <- ls(pattern=glob2rx("fluxdata*"))

# plot all variables
for (d in fluxvars){
t <- read.table(get(d)$fluxname,header=TRUE,sep=",")
unit <- get(d)$unit #"gC/m2/month"
nbands <- 12
# find rows starting with new measurement point
point.index <- which(is.na(t[,1])==FALSE)
# coordinates, names and pixelnumbers of those points
point.index.end <- c(point.index-1,length(t[,1]))[2:(length(point.index)+1)]
coord <- t[point.index,1:2]
names <- t[point.index,3]
pixelnr.out <- find.nearest.pixel(grid.out,coord$lon,coord$lat,res)
pixelnr.b <- find.nearest.pixel(grid.b,coord$lon,coord$lat,res)

# measurement startyears / months
startyears <- t[point.index,4]
startmonths <- t[point.index,5]
endyears <- t[point.index.end,4]
endyears <- ifelse((endyears-startyear)<(nyear-1),endyears,(startyear+nyear-1))
endmonths <- t[point.index.end,5]
#fluxtime <- strptime(c(paste(t[,4],t[,5],lastday[t[,5]])),format("%Y %m %d"))
fluxtime <- strptime(c(paste(t[,4],t[,5],15)),format("%Y %m %d"))
startband <- (startyears-startyear)*nbands+startmonths
endband <- (endyears-startyear)*nbands+endmonths
startm <- ifelse(min(startband)%%nbands==0,nbands,min(startband)%%nbands)
endm <- ifelse(max(endband)%%nbands==0,nbands,max(endband)%%nbands)
#LPJtime <- strptime(c(paste(as.integer(min(startband)/nbands)+startyear,startm,lastday[startm]),paste(as.integer(max(endband)/nbands)+startyear-1,endm,lastday[endm])),format("%Y %m %d"))
LPJtime <- strptime(c(paste(as.integer(min(startband)/nbands)+startyear,startm,15),paste(as.integer(max(endband)/nbands)+startyear-1,endm,15)),format("%Y %m %d"))

# extract pixeldata
if (d=="fluxdata.nep"||d=="fluxdata.rh"){
    rh.out <- extract.pixeldata(file.rh.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    rh.b <- extract.pixeldata(file.rh.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    data.out <- rh.out
    data.b <- rh.b
}
if (d=="fluxdata.nep"){
   # gpp.out <- extract.pixeldata(file.gpp.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
   #gpp.b <- extract.pixeldata(file.gpp.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    npp.out <- extract.pixeldata(file.npp.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    npp.b <- extract.pixeldata(file.npp.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    data.out <- rh.out-npp.out
    data.b <- rh.b-npp.b
}
if (d=="fluxdata.gpp"){
    data.out <- extract.pixeldata(file.gpp.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    data.b <- extract.pixeldata(file.gpp.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
}
if (d=="fluxdata.wf"){
    # extract pixeldata
    evap.out <- extract.pixeldata(file.evap.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    transp.out <- extract.pixeldata(file.transp.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    intc.out <- extract.pixeldata(file.intc.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)
    evap.b <- extract.pixeldata(file.evap.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    transp.b <- extract.pixeldata(file.transp.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    intc.b <- extract.pixeldata(file.intc.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)
    #sum of water fluxes
    data.out <- intc.out+evap.out+transp.out
    data.b <- intc.b+evap.b+transp.b
}
if(d=="fluxdata.dis"){
   data.out <- extract.pixeldata(file.dis.out,pixelnr.out,npixel.out,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.out)/1000*30
   data.b <- extract.pixeldata(file.dis.b,pixelnr.b,npixel.b,startband=min(startband),endband=max(endband),offset=offset,data.type=data.type,size=sizeof.data,endian=endian.b)/1000*30
}

# plot flux data versus LPJ data
par(par.img)
l <- layout.flux
page.layout(l)
count <- 1
# plot each pixel
for (p in 1:length(names)){
  if(count%%max(l$layout)==1){
    print.hf(par.title2,par.footer,paste(get(d)$title),date)
    par(par.title2)
    text(0,0,adj=c(0,0),lab=get(d)$des)
    par(par.img)
    plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
    if(d=="fluxdata.dis"){
      legend(0.2,1,legend=c("benchmark run","run","gauge data"),title=get(d)$title,lty=c(1,2,1),lwd=c(1,1,2),cex=1)
      rect(0.275,0.4,0.29,0.51,density=30,angle=45)#col="grey")
      rect(0.275,0.57,0.29,0.64,col="grey30")
      rect(0.275,0.7,0.29,0.77,col="grey80")
      plot.new(); plot.window(xlim=c(0,1),ylim=c(0,1))
      count <- count+1
    }else{
       legend(0.2,1,legend=c("benchmark run","run","flux data"),title=get(d)$title,lty=c(1,2,1),lwd=c(1,1,2),cex=1)
    }
    count <- count+3    
  }
  disp.ylim <- (c(min(data.out[,p],data.b[,p],t[point.index[p]:point.index.end[p],6],na.rm=TRUE),max(data.out[,p],data.b[,p],t[point.index[p]:point.index.end[p],6],na.rm=TRUE)))*1.3
  time <- fluxtime[point.index[p]:point.index.end[p]]
  plot(time,t[point.index[p]:point.index.end[p],6],lwd=2,type="l",ylim=disp.ylim,ylab=unit,xlab="")
  lines(seq(LPJtime[1],LPJtime[2],by="month"),data.out[,p],lty=2)
  title(paste(names[p],abs(coord$lat[p]),ifelse(coord$lat[p]>=0,"N","S"),"/",abs(coord$lon[p]),ifelse(coord$lon[p]>=0,"E","W")))
  lines(seq(LPJtime[1],LPJtime[2],by="month"),data.b[,p])
  # quality measures
 # r.out <- cor(t[point.index[p]:point.index.end[p],6],nep.out[match(as.numeric(time),seq(LPJtime[1],LPJtime[2],by="month")),p])
  tmp <- t[point.index[p]:point.index.end[p],6]
  tmp2 <- match(as.character(time),as.character(seq(LPJtime[1],LPJtime[2],by="month")))
  r.out <- cor(tmp[!is.na(tmp2)],data.out[tmp2[!is.na(tmp2)],p],use="pairwise.complete.obs")
  r.b <- cor(tmp[!is.na(tmp2)],data.b[tmp2[!is.na(tmp2)],p],use="pairwise.complete.obs")
  w.out <- Willmott(tmp[!is.na(tmp2)],data.out[tmp2[!is.na(tmp2)],p])
  w.b <- Willmott(tmp[!is.na(tmp2)],data.b[tmp2[!is.na(tmp2)],p])
  plot.window(xlim=c(0,1),ylim=c(0,1))
  text(0,0.95,paste("R² br:",formatC(r.b*r.b,digits=2,format="f")),pos=4)
  text(0,0.8,paste("R² r:",formatC(r.out*r.out,digits=2,format="f")),pos=4)
  text(0.5,0.95,paste("W br:",formatC(w.b,digits=2,format="f")),pos=4)
  text(0.5,0.8,paste("W r:",formatC(w.out,digits=2,format="f")),pos=4) 
  count <- count+1
  if(d=="fluxdata.dis"){
  # plot monthly average
    mdata <- rep(0,12)
    sdata <- rep(0,12)
    q1data <- rep(0,12)
    q2data <- rep(0,12)
    tmp <- t[point.index[p]:point.index.end[p],]
    for(month in 1:12){
      mdata[month] <- mean(tmp[tmp[,5]==month,6],na.rm=T)
      sdata[month] <- sd(tmp[tmp[,5]==month,6],na.rm=T)
      q1data[month] <- quantile(tmp[tmp[,5]==month,6],na.rm=T)[2]
      q2data[month] <- quantile(tmp[tmp[,5]==month,6],na.rm=T)[4]
    }
    
    
    disp.ylim <- (c(min(c(q1data,data.out[,p],data.b[,p]))-0.3*mean(mdata),max(c(q2data,data.out[,p],data.b[,p]))+0.3*mean(mdata)))
    plot(1:12,mdata,lwd=2,type="l",ylim=disp.ylim,ylab=unit,xlab="")
    #rect((1:12)-0.375,q1data,(1:12)-0.125,q2data,col="grey")
    rect((1:12)-0.33,q1data,(1:12)+0.33,q2data,density=25,angle=45)#col="dark grey")
    sum.d <- sum(mdata)
    
    tmp <- data.out[(startband[p]-min(startband)):(endband[p]-min(startband)+1),p]
    tmp2 <- c(seq(startmonths[p],12),rep(1:12,endyears[p]-startyears[p]-1),seq(1,endmonths[p]))
    for(month in 1:12){
      mdata[month] <- mean(tmp[tmp2==month],na.rm=T)
      sdata[month] <- sd(tmp[tmp2==month],na.rm=T)
      q1data[month] <- quantile(tmp[tmp2==month],na.rm=T)[2]
      q2data[month] <- quantile(tmp[tmp2==month],na.rm=T)[4]
    }
    #plot.window(xlim=c(0,1),ylim=c(0,1))
    sum.r <- sum(mdata)
    
    lines(1:12,mdata,lty=2)
    rect((1:12)-0.1,q1data,(1:12)+0.1,q2data,col="grey30")#density=15,angle=90)#col="dark grey")
    title(paste(names[p],abs(coord$lat[p]),ifelse(coord$lat[p]>=0,"N","S"),"/",abs(coord$lon[p]),ifelse(coord$lon[p]>=0,"E","W")))
    tmp <- data.b[(startband[p]-min(startband)):(endband[p]-min(startband)+1),p]
    tmp2 <- c(seq(startmonths[p],12),rep(1:12,endyears[p]-startyears[p]-1),seq(1,endmonths[p]))
    for(month in 1:12){
      mdata[month] <- mean(tmp[tmp2==month],na.rm=T)
      sdata[month] <- sd(tmp[tmp2==month],na.rm=T)
      q1data[month] <- quantile(tmp[tmp2==month],na.rm=T)[2]
      q2data[month] <- quantile(tmp[tmp2==month],na.rm=T)[4]
    }
    lines(1:12,mdata)
    rect((1:12)-0.1,q1data,(1:12)+0.1,q2data,col="grey80")#density=30,angle=135)#,col="light grey")
    plot.window(xlim=c(0,1),ylim=c(0,1))
    text(0.5,0.95,paste("sum br:",formatC(sum(mdata),digits=2,format="f")),pos=4)
    text(0,0.95,paste("sum data:",formatC(sum.d,digits=2,format="f")),pos=4)
    text(0.5,0.8,paste("sum r:",formatC(sum.r,digits=2,format="f")),pos=4)
    count <- count+1
  }#discharge
} #endfor (each point)

}#endfor (flux data)

# close read only files
for (fnr in names(which(showConnections()[,3]=="rb")))  close(getConnection(as.integer(fnr)))

if(freemem==TRUE)
  rm(t,point.index,point.index.end,coord,names,pixelnr.out,pixelnr.b,startyears,startmonths,endyears,endmonths,fluxtime,startband,endband,startm,endm,LPJtime,npp.out,rh.out,npp.b,rh.b,nep.out,nep.b,time,evap.out,transp.out,intc.out,evap.b,transp.b,intc.b,wf.out,wf.b)

#-------------------------------------------------------------------
# PART 5
# Yield comparison with FAO data
#-------------------------------------------------------------------
if(isnatural==0){
  par(par.img)
  l <- layout.sums
  page.layout(l)
  count <- 1
  fao.yields <- array(0,dim=c(ncft,nreg))
  # read in FAO to LPJmL country code key
  country.key <- read.table(fao.countryfile,header=T)
  fao2lpjml.country <- array(-9,n.country.fao)
  for(i in 1:n.country.fao){
    for(j in 1:n.country.lpjml){
      if(i==country.key$Country_Code_FAO[j]){
        fao2lpjml.country[i] <- j
        break()
      }
    }
  }
  # reading FAO data
  for(i in fao.years){
    fao.data <- read.csv2(paste(fao.path,"fao",i,"_yields_in_hgha.csv",sep=""),header=T)
    fao.data[is.na(fao.data)] <- 0
    for(k in 1:length(fao.data$country.codes)){
      if(fao.data$country.codes[k]>0){
        for(r in 1:n.country.lpjml){
          if(r==fao2lpjml.country[fao.data$country.codes[k]]){
            fao.yields[1,r] <- fao.yields[1,r] + fao.data$Wheat[k]
            fao.yields[2,r] <- fao.yields[2,r] + fao.data$Rice..paddy[k]
            fao.yields[3,r] <- fao.yields[3,r] + fao.data$Maize[k]
            fao.yields[4,r] <- fao.yields[4,r] + fao.data$Millet[k]      
            fao.yields[5,r] <- fao.yields[5,r] + fao.data$Peas..dry[k]
            fao.yields[6,r] <- fao.yields[6,r] + fao.data$Sugar.beet[k]
            fao.yields[7,r] <- fao.yields[7,r] + fao.data$Cassava[k]
            fao.yields[8,r] <- fao.yields[8,r] + fao.data$Sunflower.seed[k]
            fao.yields[9,r] <- fao.yields[9,r] + fao.data$Soybeans[k]
            fao.yields[10,r] <- fao.yields[10,r] + fao.data$Groundnuts..with.shell[k]
            fao.yields[11,r] <- fao.yields[11,r] + fao.data$Rapeseed[k]
            fao.yields[12,r] <- fao.yields[12,r] + fao.data$Sugar.cane[k]
          }
        }
      }
    }# endfor (fao countries)
  }# endfor (fao years)
  fao.yields <- fao.yields/length(fao.years)/10000 # averaging and converstion to t/ha
  
  # read LPJ country codes
  cow.file <- file(cow.fn,"rb")
  seek(cow.file,where=39,start="origin")
  cow.data <- matrix(readBin(cow.file,integer(),size=2,n=npixel.in*2,endian=endian.in),ncol=2,byrow=TRUE)
  close(cow.file)
  region.out <- cow.data[index.out,1]+1
  region.b <- cow.data[index.b,1]+1
  region <- rbind(region.b,region.out)
  names <- c("benchmark run","run")
  pixel <- c(npixel.b,npixel.out)
  # read LPJ yield data
  for (b in cropbands){ 
    n <- 1
    for(path in c(path.b,path.out)){
      file.yield.lpj <- file(paste(path,harvest.fn,sep=""),"rb")
      file.cropfrac <- file(paste(path,harea.fn,sep=""),"rb")
      npixel <- pixel[n]
      # initializations
      rainfed <- rep(0,npixel)
      irrigated <- rep(0,npixel)
      rainfed.cropfrac <- rep(0,npixel)
      irrigated.cropfrac <- rep(0,npixel)
      region.rainfed <- rep(0,nreg)
      region.irrigated <- rep(0,nreg)
      region.rainfed.croparea <- rep(0,nreg)
      region.irrigated.croparea <- rep(0,nreg)
      region.total.croparea <- rep(0,nreg)
      region.average <- rep(0,nreg)
      # average of LPJ data over all years with fao data
      for (y in (fao.years-startyear)){    
        seek(file.yield.lpj, where=y*nbands.crops*npixel*sizeof.data+(b-1)*npixel*sizeof.data,origin="start")
        buf <- readBin(file.yield.lpj,data.type,size=sizeof.data,n=npixel)
        rainfed <- rainfed  + buf/0.45*0.01*FRESHMATTER[b] #t FM/ha
        seek(file.cropfrac, where=y*nbands.crops*npixel*sizeof.data+(b-1)*npixel*sizeof.data,origin="start")
        buf <- readBin(file.cropfrac,data.type,size=sizeof.data,n=npixel)
        rainfed.cropfrac <- rainfed.cropfrac  + buf
        band2 <- b+13
        seek(file.yield.lpj, where=y*nbands.crops*npixel*sizeof.data+(band2-1)*npixel*sizeof.data,origin="start")
        buf <- readBin(file.yield.lpj,data.type,size=sizeof.data,n=npixel)
        irrigated <- irrigated + buf/0.45*0.01*FRESHMATTER[b] #t FM/ha
        seek(file.cropfrac, where=y*nbands.crops*npixel*sizeof.data+(band2-1)*npixel*sizeof.data,origin="start")
        buf <- readBin(file.cropfrac,data.type,size=sizeof.data,n=npixel)
        irrigated.cropfrac <- irrigated.cropfrac + buf    
      }#endfor (each year)
 
      rainfed <- rainfed/length(fao.years)
      irrigated <- irrigated/length(fao.years)
      rainfed.cropfrac <- rainfed.cropfrac/length(fao.years)
      irrigated.cropfrac <- irrigated.cropfrac/length(fao.years)
      close(file.yield.lpj)
      close(file.cropfrac)
    
      # multiply yields with gridcell fraction and area
      for(p in 1:npixel){
        if(region[n,p]>0){
          region.rainfed[region[n,p]] <- region.rainfed[region[n,p]]+rainfed[p]*rainfed.cropfrac[p]*grid.out$area[p]/10000
          region.irrigated[region[n,p]] <- region.irrigated[region[n,p]]+irrigated[p]*irrigated.cropfrac[p]*grid.out$area[p]/10000
          if(NOIRRIG==TRUE){
            region.average[region[n,p]] <- region.average[region[n,p]]+(rainfed[p]*rainfed.cropfrac[p]+rainfed[p]*irrigated.cropfrac[p])*grid.out$area[p]/10000
          } else {
            region.average[region[n,p]] <- region.average[region[n,p]]+(rainfed[p]*rainfed.cropfrac[p]+irrigated[p]*irrigated.cropfrac[p])*grid.out$area[p]/10000
          }
          region.rainfed.croparea[region[n,p]] <- region.rainfed.croparea[region[n,p]] +  rainfed.cropfrac[p]*grid.out$area[p]/10000
          region.irrigated.croparea[region[n,p]] <- region.irrigated.croparea[region[n,p]] + irrigated.cropfrac[p]*grid.out$area[p]/10000
          region.total.croparea[region[n,p]] <- region.total.croparea[region[n,p]] + (rainfed.cropfrac[p]+irrigated.cropfrac[p])*grid.out$area[p]/10000
        } #endif (region>0)
      }#endfor (each pixel)
      region.average <- region.average/region.total.croparea
      region.average[is.nan(region.average)] <- 0
      onetoone <- array(0,2)
      onetoone[1] <- min(region.average,fao.yields[b,])
      onetoone[2] <- max(region.average,fao.yields[b,])
      #plot
      if(count%%max(l$layout)==1){
        print.hf(par.title2,par.footer,"Yields FAO vs LPJmL",date)
        count <- count+2
      }
      tit <- paste(cropnames[b],",",names[n],fao.years[1],"-",fao.years[length(fao.years)])
      plot(fao.yields[b,region.average>0],region.average[region.average>0],main=tit,xlab="FAO yields [t FM/ha]", ylab="LPJmL yields [t FM/ha]",col="green",pch=3,lwd=0.8)
      count <- count+1
      circle.radius <- (region.total.croparea/max(region.total.croparea)*max(fao.yields[b,])/5)
      circle.radius[is.infinite(circle.radius)] <- 0
      symbols(fao.yields[b,],region.average,circles=circle.radius,add=T,cex=0.5,inches=F)
      points(fao.yields[b,region.average>fao.yields[b,]],region.average[region.average>fao.yields[b,]],col="green",pch=3,lwd=0.8)
      points(fao.yields[b,region.average/fao.yields[b,]>2], region.average[region.average/fao.yields[b,]>2],col="blue",pch=3,lwd=0.8)
      points(fao.yields[b,region.average/fao.yields[b,]<0.5], region.average[region.average/fao.yields[b,]<0.5],col="red",pch=3,lwd=0.8)
      #points(fao.yields[b,region.average==0],region.average[region.average==0],col="orange",pch=3,lwd=0.8)
      lines(onetoone,onetoone,lwd=1)
      legend("bottomright",legend=c("acceptable","strong overestimation (>200%)","underestimation (<50%)"),col=c("green","blue","red","orange"),pch=1,pt.lwd=1,pt.cex=1,cex=0.7)
      # quality measures
      r <- cor(fao.yields[b,region.average>0],region.average[region.average>0],use="pairwise.complete.obs")
      w <- Willmott(fao.yields[b,region.average>0],region.average[region.average>0])  
      plot.window(xlim=c(0,1),ylim=c(0,1))
      text(0,0.95,paste("R²:",formatC(r*r,digits=2,format="f")),pos=4)
      text(0,0.85,paste("W:",formatC(w,digits=2,format="f")),pos=4) 
      n <- n+1
    }#endfor (each file)
  }#endfor (each band)
}#end (!isnatural)

# close pdf file
dev.off()

# compress pdf file, requires pdftk
if(compress.pdf==1){
  cmd <- paste("gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=tmp.pdf -dPDFSETTINGS=/screen tmp.ps")
  system(cmd)
  system(paste("mv tmp.pdf ",pdfname))
  system("rm tmp.ps")
}


print("Time:\n")
print(proc.time())
