########################################################################
##                                                                    ##
##               M  a  k  e  f  i  l  e                               ##
##                                                                    ##
##   Makefile for C implementation of LPJmL                           ##
##   The call of ./configure.sh copies os-specific makefile.* in      ##
##   directory config to Makefile.inc                                 ##
##                                                                    ##
##   written by Werner von Bloh                                       ##
##   Potsdam Institute for Climate Impact Research                    ##
##   P.O. Box 60 12 03                                                ##
##   14412 Potsdam/Germany                                            ##
##                                                                    ##
##   Last change: $Date:: 2015-07-22 11:01:20 +0200 (mer., 22 juil.#$ ##
##   By         : $Author:: bloh                            $         ##
##                                                                    ##
########################################################################

include Makefile.inc

INC     = include

HDRS    = $(INC)/buffer.h $(INC)/cell.h $(INC)/climate.h $(INC)/conf.h\
          $(INC)/config.h $(INC)/coord.h $(INC)/crop.h $(INC)/cropdates.h\
          $(INC)/date.h $(INC)/discharge.h $(INC)/param.h $(INC)/input.h\
          $(INC)/errmsg.h $(INC)/grass.h $(INC)/header.h $(INC)/landuse.h\
          $(INC)/list.h $(INC)/lpj.h $(INC)/manage.h $(INC)/managepar.h\
          $(INC)/numeric.h $(INC)/tree.h $(INC)/image.h $(INC)/biomes.h\
          $(INC)/output.h $(INC)/pft.h $(INC)/pftlist.h $(INC)/pftpar.h\
          $(INC)/soil.h $(INC)/soilpar.h $(INC)/stand.h $(INC)/swap.h\
          $(INC)/types.h $(INC)/units.h $(INC)/climbuf.h $(INC)/intlist.h\
          $(INC)/queue.h $(INC)/pnet.h $(INC)/channel.h $(INC)/build.h\
          $(INC)/natural.h $(INC)/grassland.h $(INC)/agriculture.h\
          $(INC)/reservoir.h $(INC)/spitfire.h $(INC)/biomass_tree.h\
          $(INC)/biomass_grass.h $(INC)/cdf.h $(INC)/outfile.h\
          $(INC)/agriculture_grass.h $(INC)/agriculture_tree.h\
	  $(INC)/nitrogen.h 


CONF	= lpjml.conf input.conf param.conf lpj.conf\
          lpjml_image.conf\
          lpjml_netcdf.conf input_netcdf.conf lpjml_fms.conf input_fms.conf

DATA    = par/*.par

CRU	= input/grid.bin input/soil.bin input/tmp2003.clm input/pre2003.clm\
          input/cld2003.clm input/co2_2003.dat input/cow_mg_2006.bin\
          input/cft1700_2005.bin input/wet2003.clm input/drainage.bin\
          input/lakes.bin input/neighb_irrigation.bin

CRU_SHUFFLE= input/soil_shuffle.bin input/landuse_shuffle.bin input/co2_2003.dat\
          input/cloud_shuffle.clm input/country_shuffle\
          input/temp_shuffle.clm input/prec_shuffle.clm input/grid_shuffle.bin

SCRIPTS	= configure.bat configure.sh bin/lpjdistribute_river bin/lpjdistribute\
          bin/newpft.sh bin/output_bsq bin/lpjsubmit_aix bin/lpjsubmit_intel\
          bin/lpjsubmit_mpich bin/lpjrun bin/backtrace bin/lpjsave\
          bin/inst_pikd_iplex.sh bin/inst_pikd_aix.sh bin/filetypes.vim\
          bin/regridlpj

FILES	= Makefile config/* AUTHORS INSTALL VERSION magic.mgc\
          $(CONF) $(DATA) $(HDRS) $(SCRIPTS)

LOADL	= loadl/lpjml_intel.jcf loadl/lpjml_aix.jcf loadl/lpjml_mpich.jcf 

main: 
	$(MKDIR) lib
	(cd src && $(MAKE))
utils:
	(cd src && $(MAKE) libs)
	(cd src/utils && $(MAKE) all)

all: main utils

install: all
	$(MKDIR) $(LPJROOT)/bin
	$(MKDIR) $(LPJROOT)/include
	$(MKDIR) $(LPJROOT)/lib
	$(MKDIR) $(LPJROOT)/par
	$(MKDIR) $(LPJROOT)/man/man1
	$(MKDIR) $(LPJROOT)/man/man5
	$(MKDIR) $(LPJROOT)/man/man3
	chmod 755 $(LPJROOT)
	chmod 755 $(LPJROOT)/bin
	chmod 755 $(LPJROOT)/include
	chmod 755 $(LPJROOT)/lib
	chmod 755 $(LPJROOT)/par
	chmod 755 $(LPJROOT)/man
	chmod 755 $(LPJROOT)/man/man1
	chmod 755 $(LPJROOT)/man/man5
	chmod 755 $(LPJROOT)/man/man3
	install bin/* $(LPJROOT)/bin
	install -m 644 $(HDRS) $(LPJROOT)/include
	install -m 644 lib/* $(LPJROOT)/lib
	install -m 644 $(DATA) $(LPJROOT)/par
	install -m 644 INSTALL VERSION AUTHORS Makefile.inc config/Makefile.template\
                       $(JCFFILE) lpjml.conf input.conf param.conf $(LPJROOT)
	install -m 644 man/whatis $(LPJROOT)/man
	install -m 644 man/man1/*.1 $(LPJROOT)/man/man1
	install -m 644 man/man5/*.5 $(LPJROOT)/man/man5
	install -m 644 man/man3/*.3 $(LPJROOT)/man/man3

test: main
	$(MKDIR) output
	$(MKDIR) restart

clean:
	(cd src  && $(MAKE) clean)

tar: 
	tar -cf lpjml-3.5.003.tar $(FILES) src/Makefile src/*.c\
	    src/climate/Makefile src/climate/*.c\
            man/man1/*.1 man/man3/*.3 man/man5/*.5 man/whatis\
            man/man1/Makefile man/man3/Makefile man/man5/Makefile man/Makefile\
	    src/crop/*.c src/crop/Makefile src/grass/*.c src/grass/Makefile\
	    src/image/Makefile src/image/*.c\
	    src/landuse/*.c src/landuse/Makefile src/lpj/*.c src/lpj/Makefile\
	    src/numeric/*.c src/numeric/Makefile src/soil/*.c src/soil/Makefile\
	    src/tools/*.c src/tools/Makefile src/tree/*.c src/tree/Makefile\
            $(LOADL)\
            src/lpj/FILES src/pnet/*.c src/pnet/FILES src/socket/Makefile\
            src/socket/*.c html/*.html html/*.css src/oracle/database.h src/oracle/db2clm.c\
            src/oracle/database.pc src/oracle/Makefile src/reservoir/Makefile\
            src/image/Makefile src/image/*.c src/reservoir/*.c\
            src/pnet/Makefile REFERENCES doc/* src/utils/*.c src/utils/Makefile\
            src/spitfire/Makefile src/spitfire/*.c R/*.r src/netcdf/Makefile src/netcdf/*.c
	    gzip -f lpjml-3.5.003.tar

zipfile: 
	zip -l lpjml-3.5.003.zip $(FILES) src/Makefile src/*.c\
	    src/climate/Makefile src/climate/*.c config/* man/* man/man1/*.1\
            man/man3/*.3 man/man5/*.5\
	    src/crop/*.c src/crop/Makefile src/grass/*.c src/grass/Makefile\
	    src/image/Makefile src/image/*.c R/*.r\
	    src/landuse/*.c src/landuse/Makefile src/lpj/*.c src/lpj/Makefile\
	    src/numeric/*.c src/numeric/Makefile src/soil/*.c src/soil/Makefile\
	    src/tools/*.c src/tools/Makefile src/tree/*.c src/tree/Makefile\
            $(LOADL)\
            src/lpj/FILES src/pnet/*.c src/pnet/FILES src/socket/Makefile\
            src/socket/*.c src/oracle/database.h src/oracle/db2clm.c\
            src/oracle/database.pc src/oracle/Makefile src/reservoir/Makefile\
            src/image/*.c src/image/Makefile src/reservoir/*.c\
            src/pnet/Makefile REFERENCES doc src/utils/*.c src/utils/Makefile\
            src/spitfire/Makefile src/spitfire/*.c src/netcdf/Makefile src/netcdf/*.c

cru.zip: $(CRU)
	zip cru.zip $(CRU)

cru_shuffle.tar: $(CRU_SHUFFLE)
	tar -cf cru_shuffle.tar $(CRU_SHUFFLE)
	gzip -f cru_shuffle.tar

cru.tar: $(CRU)
	tar -cf cru.tar $(CRU)
	gzip -f cru.tar
