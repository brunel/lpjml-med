########################################################################
##                                                                    ##
##               M  a  k  e  f  i  l  e  .  g  c  c                   ##
##                                                                    ##
##   Make include file for compiler and os specific settings          ##
##   Compile and link options for GNU C compiler gcc                  ##
##                                                                    ##
##   written by Werner von Bloh                                       ##
##   Potsdam Institute for Climate Impact Research                    ##
##   P.O. Box 60 12 03                                                ##
##   14412 Potsdam/Germany                                            ##
##                                                                    ##
##   Last change: $Date:: 2015-10-28 10:14:19 +0100 (mer., 28 oct. #$ ##
##   By         : $Author:: bloh                            $         ##
##                                                                    ##
########################################################################

CC	= mpiicc
DEBUGFLAGS= -g
WFLAG = -Wall
LPJFLAGS= -DSAFE -DUSE_RAND48 -DUSE_MPI -DWITH_FPE -DUSE_TGAMMA -DUSE_NETCDF -DUSE_NETCDF4 # -DDAILY_ESTABLISHMENT
NETCDF_INC      = -I$(NETCDF_CROOT)/include -I$(UDUNITSROOT)/include
NETCDF_LIB      = -L$(NETCDF_CROOT)/lib -L$(UDUNITSROOT)/lib -L$(EXPATROOT)/lib -L$(HDF5ROOT)/lib -L$(CURLROOT)/lib -L$(OPENSSLROOT)/lib -L$(SZIPROOT)/lib
OPTFLAGS  = -O2
O	= o
A	= a
E	=
AR	= ar
ARFLAGS	= r 
RM	= rm 
RMFLAGS	= -f
LIBS	= -lm $(NETCDF_LIB) -lnetcdf
LINK	= icc
LINKMAIN= mpiicc
MKDIR	= mkdir -p
SLASH	= /
X11LIB	= -L/usr/lib -lX11
VIEWPRG	= lpjliveview
CFLAGS	= $(WFLAG) $(LPJFLAGS) $(OPTFLAGS)
LNOPTS	= $(WFLAG) $(OPTFLAGS) -o 
LPJROOT = /home/brunel/lpjml-med
